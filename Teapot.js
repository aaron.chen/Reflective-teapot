var gl;
var canvas;
var shaderProgram;

// Create a place to store the Teapot vertex Buffer
var teaVertexBuffer;
var teaVertexN = 0;

// Create a place to store the Teapot Index Buffer
var teaIndexBuffer;
var teaFaceN = 0;

var teaNormalBuffer;

// Create a place to store the texture coords for the mesh
var cubeTCoordBuffer;

// Create a place to store terrain geometry
var cubeVertexBuffer;

// Create a place to store the triangles
var cubeTriIndexBuffer;

// Create a place to store the Normals
var cubeNormalBuffer;

// Create a place to store the texture
var cubeImages = [];
var cubeTexture;

// View parameters
var eyePt = vec3.fromValues(0.0,0.0,1.0);
var viewDir = vec3.fromValues(0.0,0.0,-1.0);
var up = vec3.fromValues(0.0,1.0,0.0);
var viewPt = vec3.fromValues(0.0,0.0,0.0);
var lightPt = vec3.fromValues(1.0,1.0,1.0);

// Create the normal
var nMatrix = mat3.create();

// Create ModelView matrix
var mvMatrix = mat4.create();

//Create Projection matrix
var pMatrix = mat4.create();

var inverseViewTransform = mat3.create(); 

var mvMatrixStack = [];

//Held down keys for interactivity
var currentlyPressedKeys= {}

//default params
var DEFAULT_SPEED = 0.035;
var DEFAULT_ACCEL = DEFAULT_SPEED / 20;
var DEFAULT_ROT_SPEED = 0.025;
var DEFAULT_PIT_SPEED = 0.005;

//init
//speed of viewpoint
var speed = DEFAULT_SPEED;
var accel = DEFAULT_ACCEL; 

//rolling parameters + relevant quaternions
var rotationSpeed = DEFAULT_ROT_SPEED;
var rotationQuatLeft = quat.create();
var rotationQuatRight = quat.create();

//pitching parameters + relevant quaternions
var pitchSpeed = DEFAULT_PIT_SPEED;
var rotationQuatUp = quat.create();
var rotationQuatDown = quat.create();
var sidewaysVec = vec3.create();
var teasidewaysVec = vec3.create();

var tea_y_rotation = 0;
var y_rotation = 0;

var loaded = 0;

/**
 * Draw a cube based on buffers.
 */
function drawCube(){

  // Draw the cube by binding the array buffer to the cube's vertices
  // array, setting attributes, and pushing it to GL.

  gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

  // Set the texture coordinates attribute for the vertices.

  // gl.bindBuffer(gl.ARRAY_BUFFER, cubeTCoordBuffer);
  // gl.vertexAttribPointer(shaderProgram.texCoordAttribute, 2, gl.FLOAT, false, 0, 0);

 // Specify the texture to map onto the faces.

  gl.activeTexture(gl.TEXTURE1);
  gl.bindTexture(gl.TEXTURE_CUBE_MAP, cubeTexture);
  gl.uniform1i(shaderProgram.uCubeSampler, 1);


	gl.uniform1i(shaderProgram.uIsCube, true);
	gl.uniform1i(shaderProgram.uToReflect, false);

   // gl.uniform1i(cubeshaderProgram.uCubeSampler, 1);

  	// Bind normal buffer
	gl.bindBuffer(gl.ARRAY_BUFFER, cubeNormalBuffer);
	gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, 3, gl.FLOAT, false, 0, 0);   

  // Draw the cube.

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeTriIndexBuffer);
  setMatrixUniforms();
  gl.drawElements(gl.TRIANGLES, 36, gl.UNSIGNED_SHORT, 0);
  // gl.drawArrays(gl.TRIANGLES, 0, 36);

	setMatrixUniforms();

	// console.log(shaderProgram);
}

/**
 * Draw a teapot_0 based on buffers.
 */
function drawTeapot(){

	// gl.useProgram(teapotshaderProgram);
	// Draw the cube by binding the array buffer to the cube's vertices
	// array, setting attributes, and pushing it to GL.
	gl.bindBuffer(gl.ARRAY_BUFFER, teaVertexBuffer);
	gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

  	// Bind normal buffer
	gl.bindBuffer(gl.ARRAY_BUFFER, teaNormalBuffer);
	gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, 3, gl.FLOAT, false, 0, 0);   

	  // gl.activeTexture(gl.TEXTURE0);
  // gl.bindTexture(gl.TEXTURE_CUBE_MAP, cubeTexture);
  // gl.uniform1i(teapotshaderProgram.uCubeSampler, 1);


	gl.uniform1i(shaderProgram.uIsCube, false);

	// var toReflect = $('#to_reflect').is(":checked"); 
	gl.uniform1i(shaderProgram.uToReflect, document.getElementById("to_reflect").checked);

	// var toBP = $('#to_bp').is(":checked"); 
	gl.uniform1i(shaderProgram.uToBP, document.getElementById("to_bp").checked);

	// Draw the cube.
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, teaIndexBuffer);
	setMatrixUniforms();
	gl.drawElements(gl.TRIANGLES, teaFaceN, gl.UNSIGNED_SHORT, 0);
	
	// gl.drawArrays(gl.TRIANGLES, 0, 36);
}

//-------------------------------------------------------------------------
/**
 * Sends Modelview matrix to shader
 */
function uploadModelViewMatrixToShader() {
	gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
}

//-------------------------------------------------------------------------
/**
 * Sends projection matrix to shader
 */
function uploadProjectionMatrixToShader() {
	gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
}

//-------------------------------------------------------------------------
/**
 * Generates and sends the normal matrix to the shader
 */
function uploadNormalMatrixToShader() {
	// mat3.normalFromMat4(cubenMatrix,cubemvMatrix);
	mat3.normalFromMat4(nMatrix,mvMatrix);

	// mat3.fromMat4(nMatrix,mvMatrix);	

	// mat3.transpose(nMatrix,nMatrix);

	// mat3.invert(nMatrix,nMatrix);

	gl.uniformMatrix3fv(shaderProgram.nMatrixUniform, false, nMatrix);
}

//----------------------------------------------------------------------------------
/**
 * Pushes matrix onto modelview matrix stack
 */
function mvPushMatrix() {
	var copy = mat4.clone(mvMatrix);
	mvMatrixStack.push(copy);
}


//----------------------------------------------------------------------------------
/**
 * Pops matrix off of modelview matrix stack
 */
function mvPopMatrix() {
	if (mvMatrixStack.length == 0) {
		throw "Invalid popMatrix!";
	}
	mvMatrix = mvMatrixStack.pop();
}

//----------------------------------------------------------------------------------
/**
 * Sends projection/modelview matrices to shader
 */
function setMatrixUniforms() {
	uploadModelViewMatrixToShader();
	uploadNormalMatrixToShader();
	uploadProjectionMatrixToShader();

    gl.uniformMatrix3fv(shaderProgram.inverseViewTransform, false, inverseViewTransform);
}

//----------------------------------------------------------------------------------
/**
 * Translates degrees to radians
 * @param {Number} degrees Degree input to function
 * @return {Number} The radians that correspond to the degree input
 */
function degToRad(degrees) {
	return degrees * Math.PI / 180;
}

//----------------------------------------------------------------------------------
/**
 * Creates a context for WebGL
 * @param {element} canvas WebGL canvas
 * @return {Object} WebGL context
 */
function createGLContext(canvas) {
	var names = ["webgl", "experimental-webgl"];
	var context = null;
	for (var i=0; i < names.length; i++) {
		try {
			context = canvas.getContext(names[i]);
		} catch(e) {}
		if (context) {
			break;
		}
	}
	if (context) {
		context.viewportWidth = canvas.width;
		context.viewportHeight = canvas.height;
	} else {
		alert("Failed to create WebGL context!");
	}
	return context;
}

//----------------------------------------------------------------------------------
/**
 * Loads Shaders
 * @param {string} id ID string for shader to load. Either vertex shader/fragment shader
 */
function loadShaderFromDOM(id) {
	var shaderScript = document.getElementById(id);
	
	// If we don't find an element with the specified id
	// we do an early exit 
	if (!shaderScript) {
		return null;
	}
	
	// Loop through the children for the found DOM element and
	// build up the shader source code as a string
	var shaderSource = "";
	var currentChild = shaderScript.firstChild;
	while (currentChild) {
		if (currentChild.nodeType == 3) { // 3 corresponds to TEXT_NODE
			shaderSource += currentChild.textContent;
		}
		currentChild = currentChild.nextSibling;
	}
 
	var shader;
	if (shaderScript.type == "x-shader/x-fragment") {
		shader = gl.createShader(gl.FRAGMENT_SHADER);
	} else if (shaderScript.type == "x-shader/x-vertex") {
		shader = gl.createShader(gl.VERTEX_SHADER);
	} else {
		return null;
	}
 
	gl.shaderSource(shader, shaderSource);
	gl.compileShader(shader);
 
	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		alert(gl.getShaderInfoLog(shader));
		return null;
	} 
	return shader;
}

//----------------------------------------------------------------------------------
/**
 * Setup the fragment and vertex shaders
 */
function setupShaders() {
	vertexShader = loadShaderFromDOM("shader-vs");
	fragmentShader = loadShaderFromDOM("shader-fs");
	
	shaderProgram = gl.createProgram();
	gl.attachShader(shaderProgram, vertexShader);
	gl.attachShader(shaderProgram, fragmentShader);
	gl.linkProgram(shaderProgram);

	if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
		alert("Failed to setup shaders");
	}

	gl.useProgram(shaderProgram);

	shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aPosition");
	gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

	shaderProgram.vertexNormalAttribute = gl.getAttribLocation(shaderProgram, "aNormal");
	gl.enableVertexAttribArray(shaderProgram.vertexNormalAttribute);

	shaderProgram.uCubeSampler = gl.getUniformLocation(shaderProgram, "uCubeSampler"); 
	shaderProgram.uIsCube = gl.getUniformLocation(shaderProgram, "uIsCube"); 
	shaderProgram.uToReflect = gl.getUniformLocation(shaderProgram, "uToReflect"); 
	shaderProgram.uToBP = gl.getUniformLocation(shaderProgram, "uToBP"); 

	shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
	shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
	shaderProgram.nMatrixUniform = gl.getUniformLocation(shaderProgram, "uNMatrix");
	shaderProgram.inverseViewTransform= gl.getUniformLocation(shaderProgram, "inverseViewTransform");
	shaderProgram.uniformLightPositionLoc = gl.getUniformLocation(shaderProgram, "uLightPosition");    
	shaderProgram.uniformAmbientLightColorLoc = gl.getUniformLocation(shaderProgram, "uAmbientLightColor");  
	shaderProgram.uniformDiffuseLightColorLoc = gl.getUniformLocation(shaderProgram, "uDiffuseLightColor");
	shaderProgram.uniformSpecularLightColorLoc = gl.getUniformLocation(shaderProgram, "uSpecularLightColor");
	shaderProgram.uniformDiffuseMaterialColor = gl.getUniformLocation(shaderProgram, "uDiffuseMaterialColor");
	shaderProgram.uniformAmbientMaterialColor = gl.getUniformLocation(shaderProgram, "uAmbientMaterialColor");
	shaderProgram.uniformSpecularMaterialColor = gl.getUniformLocation(shaderProgram, "uSpecularMaterialColor");
	shaderProgram.uniformShininess = gl.getUniformLocation(shaderProgram, "uShininess"); 
	// shaderProgram.uTheta = gl.getUniformLocation(shaderProgram, "uTheta"); 
	shaderProgram.uViewPt = gl.getUniformLocation(shaderProgram, "uViewPt"); 

}

//-------------------------------------------------------------------------
/**
 * Sends material information to the shader
 * @param {Float32Array} a diffuse material color
 * @param {Float32Array} a ambient material color
 * @param {Float32Array} a specular material color 
 * @param {Float32} the shininess exponent for Phong illumination
 */
function uploadMaterialToShader(dcolor, acolor, scolor,shiny) {
  gl.uniform3fv(shaderProgram.uniformDiffuseMaterialColor, dcolor);
  gl.uniform3fv(shaderProgram.uniformAmbientMaterialColor, acolor);
  gl.uniform3fv(shaderProgram.uniformSpecularMaterialColor, scolor);
    
  gl.uniform1f(shaderProgram.uniformShininess, shiny);
}

//-------------------------------------------------------------------------
/**
 * Sends light information to the shader
 * @param {Float32Array} loc Location of light source
 * @param {Float32Array} a Ambient light strength
 * @param {Float32Array} d Diffuse light strength
 * @param {Float32Array} s Specular light strength
 */
function uploadLightsToShader(loc,a,d,s) {
	gl.uniform3fv(shaderProgram.uniformLightPositionLoc, loc);
	gl.uniform3fv(shaderProgram.uniformAmbientLightColorLoc, a);
	gl.uniform3fv(shaderProgram.uniformDiffuseLightColorLoc, d);
	gl.uniform3fv(shaderProgram.uniformSpecularLightColorLoc, s);
}

//----------------------------------------------------------------------------------
/**
 * Populate buffers with data
 */
function setupBuffers() {
	readTextFile("teapot_0.obj", setupTeapotBuffers);


  // Create a buffer for the cube's vertices.

  cubeVertexBuffer = gl.createBuffer();

  // Select the cubeVerticesBuffer as the one to apply vertex
  // operations to from here out.

  gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexBuffer);

  // Now create an array of vertices for the cube.

  var vertices = [

    // Right face
     1.0, -1.0, -1.0,
     1.0,  1.0, -1.0,
     1.0,  1.0,  1.0,
     1.0, -1.0,  1.0,

    // Left face
    -1.0, -1.0, -1.0,
    -1.0, -1.0,  1.0,
    -1.0,  1.0,  1.0,
    -1.0,  1.0, -1.0,

    // Top face
    -1.0,  1.0, -1.0,
    -1.0,  1.0,  1.0,
     1.0,  1.0,  1.0,
     1.0,  1.0, -1.0,

    // Bottom face
    -1.0, -1.0, -1.0,
     1.0, -1.0, -1.0,
     1.0, -1.0,  1.0,
    -1.0, -1.0,  1.0,

    // Front face
    -1.0, -1.0,  1.0,
     1.0, -1.0,  1.0,
     1.0,  1.0,  1.0,
    -1.0,  1.0,  1.0,

    // Back face
    -1.0, -1.0, -1.0,
    -1.0,  1.0, -1.0,
     1.0,  1.0, -1.0,
     1.0, -1.0, -1.0	
  ];

  // Now pass the list of vertices into WebGL to build the shape. We
  // do this by creating a Float32Array from the JavaScript array,
  // then use it to fill the current vertex buffer.

  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

  // Map the texture onto the cube's faces.

  cubeTCoordBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, cubeTCoordBuffer);

  var textureCoordinates = [
    // Front
    0.0,  0.0,
    1.0,  0.0,
    1.0,  1.0,
    0.0,  1.0,
    // Back
    0.0,  0.0,
    1.0,  0.0,
    1.0,  1.0,
    0.0,  1.0,
    // Top
    0.0,  0.0,
    1.0,  0.0,
    1.0,  1.0,
    0.0,  1.0,
    // Bottom
    0.0,  0.0,
    1.0,  0.0,
    1.0,  1.0,
    0.0,  1.0,
    // Right
    0.0,  0.0,
    1.0,  0.0,
    1.0,  1.0,
    0.0,  1.0,
    // Left
    0.0,  0.0,
    1.0,  0.0,
    1.0,  1.0,
    0.0,  1.0
  ];

  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoordinates),
                gl.STATIC_DRAW);

  // Build the element array buffer; this specifies the indices
  // into the vertex array for each face's vertices.

  cubeTriIndexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeTriIndexBuffer);

  // This array defines each face as two triangles, using the
  // indices into the vertex array to specify each triangle's
  // position.

  var cubeVertexIndices = [
    0,  1,  2,      0,  2,  3,    // front
    4,  5,  6,      4,  6,  7,    // back
    8,  9,  10,     8,  10, 11,   // top
    12, 13, 14,     12, 14, 15,   // bottom
    16, 17, 18,     16, 18, 19,   // right
    20, 21, 22,     20, 22, 23    // left
  ]

  // Now send the element array to GL

  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,
      new Uint16Array(cubeVertexIndices), gl.STATIC_DRAW);

  cubeNormalBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, cubeNormalBuffer);

  var cubeNorms = vertices.map(function () { return 0 });
  setNormals(cubeNorms,cubeVertexIndices, vertices);
    // Now send the element array to GL


  gl.bufferData(gl.ARRAY_BUFFER,
      new Float32Array(cubeNorms), gl.STATIC_DRAW);
}

function setupTeapotBuffers(text){
	var lines = text.split("\n");
	var vertices = [];
	var faces = [];

	for (var i = 0; i < lines.length; i++){
		var line = lines[i];
		var vals = line.split(/ +/)
		if (line[0] === '#' || line[0] === 'g'){
			continue;
		}
		if (line[0] === 'v'){
			vertices.push(parseFloat(vals[1]))
			vertices.push(parseFloat(vals[2]))
			vertices.push(parseFloat(vals[3]))
			teaVertexN += 3;
		}
		else if (line[0] === 'f'){
			faces.push(parseInt(vals[1]) - 1)
			faces.push(parseInt(vals[2]) - 1)
			faces.push(parseInt(vals[3]) - 1)
			teaFaceN += 3;
		}

		// console.log(vals.length)
	}
	// console.log(teaVertexN)
	// console.log(vertices)
	// console.log(teaFaceN)
	// console.log(faces)

	teaVertexBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, teaVertexBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

	teaIndexBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, teaIndexBuffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(faces), gl.STATIC_DRAW);


	teaNormalBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, teaNormalBuffer);
	var teaNorms = vertices.map(function () { return 0 });
	setNormals(teaNorms, faces, vertices);
    // Now send the element array to GL

	// console.log(teaNorms);

	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(teaNorms), gl.STATIC_DRAW);
}

//----------------------------------------------------------------------------------
/**
 * Draw call that applies matrix transformations to model and draws model in frame
 */
function draw() { 
	var transformVec = vec3.create();

	gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	// We'll use perspective 
	mat4.perspective(pMatrix,degToRad(45), gl.viewportWidth / gl.viewportHeight, 0.1, 200.0);

	// We want to look down -z, so create a lookat point in that direction    
	vec3.add(viewPt, eyePt, viewDir);
	// Then generate the lookat matrix and initialize the MV matrix to that view
	mat4.lookAt(mvMatrix,eyePt,viewPt,up);

	//Draw Terrain
	mvPushMatrix();
	mvPushMatrix();

	mat4.translate(mvMatrix, mvMatrix,transformVec);

	// mat4.rotateY(mvMatrix, mvMatrix, degToRad(y_rotation));

	// setMatrixUniforms();



	// var lightPoint = vec3.fromValues(1.0,1.0,1.0);
	// var lightRotation = mat4.create()
	// mat4.fromRotation(lightRotation, degToRad(y_rotation), vec3.fromValues(0.0,1.0,0.0))

	// vec3.transformMat4(lightPoint, lightPoint, lightRotation);
	// console.log(lightPoint);

	// uploadLightsToShader([lightPoint[0],lightPoint[1],lightPoint[2]],[0.5,0.4,0.2],[0.7,0.7,0.6],[0.5,0.7,0.9]);


	// vec3.rotateY(lightPoint, lightPoint, vec3.fromValues(0,1,0), degToRad(y_rotation));


	gl.uniform1i(shaderProgram.uTheta, degToRad(y_rotation));


	mat3.fromMat4(inverseViewTransform, mvMatrix);
    mat3.invert(inverseViewTransform,inverseViewTransform);


	// var rotatedViewPt = vec3.create();
	// vec3.rotateY(rotatedViewPt, viewDir, vec3.fromValues(0,0,0), degToRad(y_rotation))

	// gl.uniform3fv(shaderProgram.uViewPt, rotatedViewPt);



	R = 255.0/255.0;
    G = 200.0/255.0;
    B = 100.0/255.0;
	shiny = 60;
	uploadLightsToShader([lightPt[0],lightPt[1],lightPt[2]],[0.5,0.4,0.2],[0.7,0.7,0.6],[0.5,0.7,0.9]);
	uploadMaterialToShader([R,G,B],[R,G,B],[0.5,0.5,0.7],shiny);

	
	
	drawCube();
	mvPopMatrix();

	mat4.rotateY(mvMatrix, mvMatrix, degToRad(tea_y_rotation));

	mat4.rotate(mvMatrix, mvMatrix, rotationSpeed, teasidewaysVec);

	mat4.scale(mvMatrix, mvMatrix, vec3.fromValues(0.1,0.1,0.1));

	drawTeapot();
	mvPopMatrix();
}

/**
 * Creates texture for application to cube.
 */
function setupTextures() {
	var targets = [gl.TEXTURE_CUBE_MAP_POSITIVE_Z, gl.TEXTURE_CUBE_MAP_NEGATIVE_Z, gl.TEXTURE_CUBE_MAP_POSITIVE_Y, gl.TEXTURE_CUBE_MAP_NEGATIVE_Y, gl.TEXTURE_CUBE_MAP_POSITIVE_X, gl.TEXTURE_CUBE_MAP_NEGATIVE_X]
	var imgsrcs = ["canary/pos-x.png","canary/neg-x.png","canary/pos-y.png","canary/neg-y.png","canary/pos-z.png","canary/neg-z.png"];
	var target, imgsrc;
  var cubeImage;
	// Fill the texture with a 1x1 blue pixel.
	// cubeTexture = gl.createTexture();
	// gl.bindTexture(gl.TEXTURE_CUBE_MAP, cubeTexture);

  // for (var i = 0; i < 6; i = i + 1){
    // target = targets[i];
     // gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array([0, 0, 255, 255]));
   // }

	for (var i = 0; i < 6; i = i + 1){
		target = targets[i];
		imgsrc = imgsrcs[i];
		cubeImages[i] = new Image();

		cubeImages[i].onload = handleTextureLoaded;
		cubeImages[i].src = imgsrc;

	}

   // https://goo.gl/photos/SUo7Zz9US1AKhZq49
}

/**
 * @param {number} value Value to determine whether it is a power of 2
 * @return {boolean} Boolean of whether value is a power of 2
 */
function isPowerOf2(value) {
  return (value & (value - 1)) == 0;
}

/**
 * Texture handling. Generates mipmap and sets texture parameters.
 * @param {Object} image Image for cube application
 * @param {Object} texture Texture for cube application
 */
function handleTextureLoaded() {
	loaded += 1
	if (loaded == 6){
		cubeTexture = gl.createTexture();
		gl.bindTexture(gl.TEXTURE_CUBE_MAP, cubeTexture);

		for (var i = 0; i < 6; i += 1){
			console.log("handleTextureLoaded, image = " + cubeImages[i]);
			console.log(cubeImages[i]);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false); 
			gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, cubeImages[i]);
			// Check if the image is a power of 2 in both dimensions.
			if (isPowerOf2(cubeImages[i].width) && isPowerOf2(cubeImages[i].height)) {
				// Yes, it's a power of 2. Generate mips.
				console.log("Loaded power of 2 texture");
			} else {
				// No, it's not a power of 2. Turn of mips and set wrapping to clamp to edge
				gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
				gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
				gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
				console.log("Loaded non-power of 2 texture");
			}
			// gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            // gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);/
            // gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
            // gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.LINEAR);

			gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
			gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);

			// gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);

			// gl.bindTexture(gl.TEXTURE_CUBE_MAP, cubeTexture);
			// gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.REPEAT);
			// gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.REPEAT);
			// gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_R, gl.REPEAT);
			// gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
			// gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

			// gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, cubeImages[i]);


		}
		gl.generateMipmap(gl.TEXTURE_CUBE_MAP);
	}
}

//----------------------------------------------------------------------------------
/**
 * Animation to be called from tick. Updates globals and performs animation for each tick.
 */
function animate() {
	//move eye forward
	temp = vec3.create();
	vec3.scale(temp, viewDir, speed);
	vec3.add(eyePt, eyePt, temp);
}

function handleKeys(){
	// left key
	if (currentlyPressedKeys[37]){
		quat.setAxisAngle(rotationQuatLeft, up, -rotationSpeed);
		vec3.transformQuat(eyePt, eyePt, rotationQuatLeft);
		vec3.transformQuat(viewDir, viewDir, rotationQuatLeft);

		vec3.transformQuat(lightPt, lightPt, rotationQuatLeft);
	}
	// right key
	if (currentlyPressedKeys[39]){
		quat.setAxisAngle(rotationQuatRight, up, rotationSpeed);
		vec3.transformQuat(eyePt, eyePt, rotationQuatRight);
		vec3.transformQuat(viewDir, viewDir, rotationQuatRight);

		vec3.transformQuat(lightPt, lightPt, rotationQuatRight);
	}
	// up key
	if (currentlyPressedKeys[38]){
		//generate third orthogonal vector (sideways)
		//reset quat to rotate relative to sideways vector
		//apply to up and viewDir
		vec3.cross(sidewaysVec, up, viewDir);
		quat.setAxisAngle(rotationQuatUp, sidewaysVec, rotationSpeed);

		vec3.transformQuat(up, up, rotationQuatUp);
		vec3.transformQuat(eyePt, eyePt, rotationQuatUp);
		vec3.transformQuat(viewDir, viewDir, rotationQuatUp);

		vec3.transformQuat(lightPt, lightPt, rotationQuatUp);
	}
	// down key
	if (currentlyPressedKeys[40]){
		//generate third orthogonal vector (sideways)
		//reset quat to rotate relative to sideways vector
		//apply to up and viewDir
		vec3.cross(sidewaysVec, up, viewDir);
		quat.setAxisAngle(rotationQuatDown, sidewaysVec, -rotationSpeed);

		vec3.transformQuat(up, up, rotationQuatDown);
		vec3.transformQuat(eyePt, eyePt, rotationQuatDown);
		vec3.transformQuat(viewDir, viewDir, rotationQuatDown);

		vec3.transformQuat(lightPt, lightPt, rotationQuatDown);
	}

	//d key
	if (currentlyPressedKeys[68]){
		tea_y_rotation += 1;

		// quat.setAxisAngle(rotationQuatLeft, up, degToRad(-1));
		// vec3.transformQuat(lightPt, lightPt, rotationQuatLeft);
	}
	// a key
	if (currentlyPressedKeys[65]){
		tea_y_rotation -= 1;

		// quat.setAxisAngle(rotationQuatRight, up, degToRad(1));
		// vec3.transformQuat(lightPt, lightPt, rotationQuatRight);
	}
	// + or = key
	// if (currentlyPressedKeys[187]){
		// vec3.cross(teasidewaysVec, up, viewDir);
		// quat.setAxisAngle(rotationQuatDown, sidewaysVec, -rotationSpeed);
	// }
	// - key
	// if (currentlyPressedKeys[189]){
		// vec3.cross(teasidewaysVec, up, viewDir);
		// quat.setAxisAngle(rotationQuatDown, sidewaysVec, -rotationSpeed);
	// }
	// r key
	if (currentlyPressedKeys[82]){
		reset();
	}
}
 
// //manipulate viewpoint by using quarterinions (once every frame), and manipulated using keys
// function handleKeys(){
// 	// left key
// 	if (currentlyPressedKeys[37]){
// 		//reset quat and apply to up vector
// 		quat.setAxisAngle(rotationQuatLeft, viewDir, -rotationSpeed);
// 		vec3.transformQuat(up, up, rotationQuatLeft);
// 	}
// 	// right key
// 	if (currentlyPressedKeys[39]){
// 		//reset quat and apply to up vector
// 		quat.setAxisAngle(rotationQuatRight, viewDir, rotationSpeed);
// 		vec3.transformQuat(up, up, rotationQuatRight);
// 	}
// 	// up key
// 	if (currentlyPressedKeys[38]){
// 		//generate third orthogonal vector (sideways)
// 		//reset quat to rotate relative to sideways vector
// 		//apply to up and viewDir
// 		vec3.cross(sidewaysVec, up, viewDir);
// 		quat.setAxisAngle(rotationQuatUp, sidewaysVec, -pitchSpeed);
// 		vec3.transformQuat(up, up, rotationQuatUp);
// 		vec3.transformQuat(viewDir, viewDir, rotationQuatUp);
// 	}
// 	// down key
// 	if (currentlyPressedKeys[40]){
// 		//generate third orthogonal vector (sideways)
// 		//reset quat to rotate relative to sideways vector
// 		//apply to up and viewDir
// 		vec3.cross(sidewaysVec, up, viewDir);
// 		quat.setAxisAngle(rotationQuatDown, sidewaysVec, pitchSpeed);
// 		vec3.transformQuat(up, up, rotationQuatDown);
// 		vec3.transformQuat(viewDir, viewDir, rotationQuatDown);
// 	}
// 	// + or = key
// 	if (currentlyPressedKeys[187]){
// 		speed += accel
// 	}
// 	// - key
// 	if (currentlyPressedKeys[189]){
// 		if (speed - accel > 0){
// 			speed -= accel
// 		}	
// 	}
// 	// r key
// 	if (currentlyPressedKeys[82]){
// 		reset();
// 	}
// }

//set inputted key to be considered pressed down
function handleKeyDown(event) {
	currentlyPressedKeys[event.keyCode] = true;
}

//set inputted key to be considered not pressed down
function handleKeyUp(event) {
	currentlyPressedKeys[event.keyCode] = false;
}

//start from beginning
function reset() {
// View parameters
	eyePt = vec3.fromValues(0.0,0.0,1.0);
	viewDir = vec3.fromValues(0.0,0.0,-1.0);
	up = vec3.fromValues(0.0,1.0,0.0);
	viewPt = vec3.fromValues(0.0,0.0,0.0);
	lightPt = vec3.fromValues(1.0,1.0,1.0);
	
	//reset params
	speed = DEFAULT_SPEED;
	accel = DEFAULT_ACCEL;
	rotationSpeed = DEFAULT_ROT_SPEED;
	pitchSpeed = DEFAULT_PIT_SPEED;
}

//----------------------------------------------------------------------------------
/**
 * Startup function called from html code to start program.
 */
 function startup() {
	canvas = document.getElementById("myGLCanvas");
	document.onkeydown = handleKeyDown;
	document.onkeyup = handleKeyUp;
	gl = createGLContext(canvas);

	setupShaders();
	setupBuffers();
	setupTextures();

	gl.clearColor(1.0, 1.0, 1.0, 1.0);
	gl.enable(gl.DEPTH_TEST);


	tick();
}

//----------------------------------------------------------------------------------
/**
 * Tick called for every animation frame.
 */
function tick() {
	requestAnimFrame(tick);
	draw();
	handleKeys();
	// animate();
}

function readTextFile(file, callbackFunction)
{
    console.log("reading "+ file);
    var rawFile = new XMLHttpRequest();
    var allText = [];
    rawFile.open("GET", file, true);
    
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                 callbackFunction(rawFile.responseText);
                 console.log("Got text file!");
                 
            }
        }
    }
    rawFile.send(null);
}