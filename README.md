# Reflective-Teapot

(Implementation of MP2 in CS418 Fa2018 at UIUC)

Program to simulate a reflective Utah teapot using a skybox in WebGL.  To run, simply run mp3.html in Firefox, or run it using a local webserver (using node.js, brackets editor, etc.).

Controls are shown in the browser. 

![reflective_teapot_animation](/sample/reflective-teapot.gif)
