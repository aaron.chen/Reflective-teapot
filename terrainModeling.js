/**
 * Iteratively generate terrain from numeric inputs
 * @param {number} n
 * @param {number} minX Minimum X value
 * @param {number} maxX Maximum X value
 * @param {number} minY Minimum Y value
 * @param {number} maxY Maximum Y value
 * @param {Array} vertexArray Array that will contain vertices generated
 * @param {Array} faceArray Array that will contain faces generated
 * @param {Array} normalArray Array that will contain normals generated
 * @return {number}
 */
function terrainFromIteration(n, minX,maxX,minY,maxY, vertexArray, faceArray,normalArray)
{
	var deltaX=(maxX-minX)/n;
	var deltaY=(maxY-minY)/n;
	for(var i=0;i<=n;i++)
		for(var j=0;j<=n;j++)
		{
			vertexArray.push(minX+deltaX*j);
			vertexArray.push(minY+deltaY*i);
			vertexArray.push(0);
		
			//initially normals aren't calculated
			normalArray.push(0);
			normalArray.push(0);
			normalArray.push(0);
		}

	var numT=0;
	for(var i=0;i<n;i++)
		for(var j=0;j<n;j++)
		{
			var vid = i*(n+1) + j;
			faceArray.push(vid);
			faceArray.push(vid+1);
			faceArray.push(vid+n+1);
			
			faceArray.push(vid+1);
			faceArray.push(vid+1+n+1);
			faceArray.push(vid+n+1);
			numT+=2;
		}


	console.log(normalArray);

	//calculate heights of vertices and put changed heights into vertexArray
	setVerticesDiamondSquare(vertexArray, n);

	//calculate normals of vertices and place into normalArray
	setNormals(normalArray, faceArray, vertexArray);
	console.log(normalArray);

	return numT;
}

/**
 * Generates line values from faces in faceArray
 * @param {Array} faceArray array of faces for triangles
 * @param {Array} lineArray array of normals for triangles, storage location after generation
 */
function generateLinesFromIndexedTriangles(faceArray,lineArray)
{
	numTris=faceArray.length/3;
	for(var f=0;f<numTris;f++)
	{
		var fid=f*3;
		lineArray.push(faceArray[fid]);
		lineArray.push(faceArray[fid+1]);
		
		lineArray.push(faceArray[fid+1]);
		lineArray.push(faceArray[fid+2]);
		
		lineArray.push(faceArray[fid+2]);
		lineArray.push(faceArray[fid]);
	}
}

//initialize four corners of diamond square (remember grid is actually n+1 by n+1)
function setVerticesDiamondSquare(vertexArray, n) {
	//in terms of start of vertex index, calc corners
	var nw = 0;
	var ne = n*3;
	var sw = (n+1)*n*3;
	var se = (n+1)*n*3 + n*3;

	//set corners
	vertexArray[nw + 2] = 0; //nw
	vertexArray[ne + 2] = Math.random() / 2; //ne
	vertexArray[sw + 2] = Math.random() / 2; //sw
	vertexArray[se + 2] = 1; //se

	console.log(vertexArray[se + 2]);

	var randomscale = 2;//initial random scale
	var queue = [];

	diamondSquareHelper(vertexArray,  n+1, n, randomscale, nw, ne, sw, se);
}

//recursive helper for diamond square
function diamondSquareHelper(vertexArray, dim, n, randomscale, nw, ne, sw, se) {


	// console.log(queue)
	//base case
	if (n == 1){
		return;
	}

	//diamond step
	var middle = nw + (n/2)*3 + dim*(n/2)*3; // nw move half right, then half down
	var middleval = (vertexArray[nw+2] + vertexArray[ne+2] + vertexArray[sw+2] + vertexArray[se+2])/4;
	middleval += (Math.random() - 0.5)*randomscale; //avg and add randomness
	// console.log(se);
	vertexArray[middle+2] = middleval;//set middleval

	//square step
	var north = nw + (n/2)*3; //nw move half right
	var south = sw + (n/2)*3;; //sw move half right
	var west = nw + dim*(n/2)*3; //nw move half down
	var east = west + n*3; //w move full right

	//north
	//add 3 values already have, also need to consider diamond part outside of square
	var northval = vertexArray[nw+2] + vertexArray[middle+2] + vertexArray[ne+2];
	if (north < dim*3) { // edge just average 3;
		northval /= 3;
	}
	else {
		northval += vertexArray[north - dim*(n/2)*3 + 2]
		northval /= 4;
	}
	//south
	//add 3 values already have, also need to consider diamond part outside of square
	var southval = vertexArray[nw+2] + vertexArray[middle+2] + vertexArray[ne+2];
	if (south >= dim*(dim-1)*3) { // edge just average 3;
		southval /= 3;
	}
	else {
		southval += vertexArray[south + dim*(n/2)*3 + 2]
		southval /= 4;
	}
	//west
	//add 3 values already have, also need to consider diamond part outside of square
	var westval = vertexArray[nw+2] + vertexArray[middle+2] + vertexArray[sw+2];
	if (west % (dim*3) == 0) { // edge just average 3;
		westval /= 3;
	}
	else {
		westval += vertexArray[west - (n/2)*3 + 2]
		westval /= 4;
	}
	//east
	//add 3 values already have, also need to consider diamond part outside of square
	var eastval = vertexArray[ne+2] + vertexArray[middle+2] + vertexArray[se+2];
	if (east % (dim*3) == dim*3-3) { // edge just average 3;
		eastval /= 3;
	}
	else {
		eastval += vertexArray[east + (n/2)*3 +2]
		eastval /= 4;
	}

	//add randomfactors
	northval += (Math.random() - 0.5)*randomscale;
	southval += (Math.random() - 0.5)*randomscale;
	westval += (Math.random() - 0.5)*randomscale;
	eastval += (Math.random() - 0.5)*randomscale;
	vertexArray[north+2] = northval; //set square vals
	vertexArray[south+2] = southval; //set square vals
	vertexArray[west+2] = westval; //set square vals
	vertexArray[east+2] = eastval; //set square vals

	diamondSquareHelper(vertexArray, dim, n/2, randomscale/2.5, nw, north, west, middle)
	diamondSquareHelper(vertexArray, dim, n/2, randomscale/2.5, north, ne, middle, east)
	diamondSquareHelper(vertexArray, dim, n/2, randomscale/2.5, west, middle, sw, south)
	diamondSquareHelper(vertexArray, dim, n/2, randomscale/2.5, middle, east, south, se)
	
}

//calculate normals of vertices and place into normalArray
function setNormals(normalArray, faceArray, vertexArray) {
	//for each triangle face
	for(var i = 0; i < faceArray.length; i += 3) {
		
		//get triangle vertices, contained as vec3 in faceverts
		var faceverts = [];
		for (var j = 0; j < 3; j += 1) {
			var facevert = [];
			facevert[0] = vertexArray[faceArray[i+j] * 3];
			facevert[1] = vertexArray[faceArray[i+j] * 3 + 1];
			facevert[2] = vertexArray[faceArray[i+j] * 3 + 2];
			faceverts[j] = vec3.fromValues(facevert[0], facevert[1], facevert[2]);
		}
		//get two edge vectors
		var v0v1 = vec3.create();
		var v0v2 = vec3.create();
		vec3.sub(v0v1, faceverts[1], faceverts[0]);
		vec3.sub(v0v2, faceverts[2], faceverts[0]);

		//normal vector = cross product
		var vnorm = vec3.create();
		vec3.cross(vnorm, v0v1, v0v2);

		// if (isNaN(vnorm[0]) || isNaN(vnorm[1]) || isNaN(vnorm[2])){
		// 	console.log(faceArray[i])
		// 	console.log(faceArray[i+1])
		// 	console.log(faceArray[i+2])
		// 	console.log(vertexArray[faceArray[i]])
		// 	console.log(vertexArray[faceArray[i+1]])
		// 	console.log(vertexArray[faceArray[i+2]])
		// 	console.log(faceverts)
		// }
		
		//add norm to each assoc. vertex
		for (var j = 0; j < 3; j += 1) {
			normalArray[faceArray[i+j] * 3] += vnorm[0];
			normalArray[faceArray[i+j] * 3 + 1] += vnorm[1];
			normalArray[faceArray[i+j] * 3 + 2] += vnorm[2];
		}
	}
	
	//normalize vertex normals
	for(var i = 0; i < normalArray.length; i += 3){
		var normalizedvert = vec3.fromValues(normalArray[i],normalArray[i+1],normalArray[i+2]); 
		vec3.normalize(normalizedvert,normalizedvert);
		
		//make into for loop?
		normalArray[i] = normalizedvert[0];
		normalArray[i+1] = normalizedvert[1];
		normalArray[i+2] = normalizedvert[2];
	}
}